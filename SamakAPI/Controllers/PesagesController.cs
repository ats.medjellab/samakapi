﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SamakAPI.Models;

namespace SamakAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PesagesController : ControllerBase
    {
        private readonly SamakDBContext _context;

        public PesagesController(SamakDBContext context)
        {
            _context = context;
        }

        // GET: api/Pesages
        [HttpGet]
        public IEnumerable<Pesage> GetPesage()
        {
            return _context.Pesage;
        }

        // GET: api/Pesages/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPesage([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pesage = await _context.Pesage.FindAsync(id);

            if (pesage == null)
            {
                return NotFound();
            }

            return Ok(pesage);
        }

        // PUT: api/Pesages/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPesage([FromRoute] int id, [FromBody] Pesage pesage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pesage.Id)
            {
                return BadRequest();
            }

            _context.Entry(pesage).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PesageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pesages
        [HttpPost]
        public async Task<IActionResult> PostPesage([FromBody] Pesage pesage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Pesage.Add(pesage);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPesage", new { id = pesage.Id }, pesage);
        }

        // DELETE: api/Pesages/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePesage([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pesage = await _context.Pesage.FindAsync(id);
            if (pesage == null)
            {
                return NotFound();
            }

            _context.Pesage.Remove(pesage);
            await _context.SaveChangesAsync();

            return Ok(pesage);
        }

        private bool PesageExists(int id)
        {
            return _context.Pesage.Any(e => e.Id == id);
        }
    }
}