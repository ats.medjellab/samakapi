﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SamakAPI.Models
{
    public class Badge
    {
        public Badge()
        {
            Personnes = new HashSet<Personne>();
        }

        public Badge(Badge badge) : this()
        {
            NumBadge = badge.NumBadge;
            NumSerie = badge.NumSerie;
            Personnes = badge.Personnes;
        }

        public int Id { get; set; }
        public string NumBadge { get; set; }
        public string NumSerie { get; set; }
        public ICollection<Personne> Personnes { get; set; }
    }
}
