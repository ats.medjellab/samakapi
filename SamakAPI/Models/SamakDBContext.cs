﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SamakAPI.Models
{
    public class SamakDBContext : DbContext
    {
        public SamakDBContext() { }

        public SamakDBContext(DbContextOptions<SamakDBContext> options) : base(options)
        {
        }

        public virtual DbSet<Badge> Badge { get; set; }
        public virtual DbSet<Users> User { get; set; }
        public virtual DbSet<Personne> Personne { get; set; }
        public virtual DbSet<Pesage> Pesage { get; set; }
        public virtual DbSet<Role> Role { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured) { }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Badge>(entity =>
            {
                entity.ToTable("badge");
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.NumBadge).HasMaxLength(100);
                entity.Property(e => e.NumSerie).HasMaxLength(100);
            });

            modelBuilder.Entity<Pesage>(entity =>
            {
                entity.ToTable("pesage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DatePesage)
                    .HasColumnName("datePesage")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdPersonne).HasColumnName("idPersonne");

                entity.Property(e => e.IdUser).HasColumnName("idUser");

                entity.Property(e => e.Poids).HasColumnName("poids");

                entity.HasOne(d => d.IdPersonneNavigation)
                    .WithMany(p => p.Pesage)
                    .HasForeignKey(d => d.IdPersonne)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__pesage__idPerson__164452B1");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Pesages)
                    .HasForeignKey(d => d.IdUser)
                    .HasConstraintName("FK__pesage__idUser__34C8D9D1");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Nom)
                    .HasColumnName("nom")
                    .HasMaxLength(100);

                entity.Property(e => e.Prenom)
                    .HasColumnName("prenom")
                    .HasMaxLength(100);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(100);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(14);
                entity.Property(e => e.IdRole).HasColumnName("idRole");

                entity.Property(e => e.DateLogin)
                    .HasColumnName("dateLogin")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateLogout)
                    .HasColumnName("dateLogout")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.IdRoleNavigation)
                   .WithMany(p => p.Users)
                   .HasForeignKey(d => d.IdRole)
                   .HasConstraintName("FK__users__idrole__71D1E811");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Libelle)
                    .HasColumnName("libelle")
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Personne>(entity =>
            {
                entity.ToTable("personne");

                entity.HasIndex(e => e.Matricule)
                    .HasName("UQ__personne__30962D11C7DD0BF2")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");


                entity.Property(e => e.Matricule)
                    .IsRequired()
                    .HasColumnName("matricule")
                    .HasMaxLength(200);

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnName("nom")
                    .HasMaxLength(100);

                entity.Property(e => e.Prenom)
                    .HasColumnName("prenom")
                    .HasMaxLength(100);

                entity.Property(e => e.Adresse)
                    .HasColumnName("adresse")
                    .HasMaxLength(100);

                entity.Property(e => e.Cin)
                    .HasColumnName("cin")
                    .HasMaxLength(100);

                entity.Property(e => e.Cnss)
                    .HasColumnName("cnss")
                    .HasMaxLength(100);

                entity.Property(e => e.Ramed)
                    .HasColumnName("ramed")
                    .HasMaxLength(100);

                entity.Property(e => e.DateArret)
                    .HasColumnName("dateArret")
                    .HasColumnType("date");

                entity.Property(e => e.DateEmbauche)
                    .HasColumnName("dateEmbauche")
                    .HasColumnType("date");

                entity.Property(e => e.DateNaissance)
                    .HasColumnName("dateNaissance")
                    .HasColumnType("date");

                entity.Property(e => e.IdBadge).HasColumnName("IDBadge");

                entity.Property(e => e.Situation)
                    .HasColumnName("situation")
                    .HasMaxLength(100);

                entity.Property(e => e.Ville)
                    .HasColumnName("ville")
                    .HasMaxLength(100);

                entity.Property(e => e.NombreEnfant).HasColumnName("nombreEnfant");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(14);


                entity.HasOne(d => d.IdBadgeNavigation)
                    .WithMany(p => p.Personnes)
                    .HasForeignKey(d => d.IdBadge)
                    .HasConstraintName("FK__personne__IDBadg__49C3F6B7");
            });

        }
    }
}
