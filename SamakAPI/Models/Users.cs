﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SamakAPI.Models
{
    public class Users
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public int? IdRole { get; set; }
        public DateTime? DateLogin { get; set; }
        public DateTime? DateLogout { get; set; }
        public Role IdRoleNavigation { get; set; }
        public ICollection<Pesage> Pesages { get; set; }
    }
}
