﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SamakAPI.Models
{
    public class Personne
    {
        public int Id { get; set; }
        public string Matricule { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Phone { get; set; }
        public string Cin { get; set; }
        public string Cnss { get; set; }
        public string Ramed { get; set; }
        public DateTime? DateNaissance { get; set; }
        public string Situation { get; set; }
        public string Ville { get; set; }
        public string Adresse { get; set; }
        public int? NombreEnfant { get; set; }
        public DateTime? DateEmbauche { get; set; }
        public DateTime? DateArret { get; set; }
        public int? IdBadge { get; set; }
        public Badge IdBadgeNavigation { get; set; }
        public ICollection<Pesage> Pesage { get; set; }

    }
}
