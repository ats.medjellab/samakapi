﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SamakAPI.Models
{
    public class Pesage
    {
        public int Id { get; set; }
        public int IdPersonne { get; set; }
        public double Poids { get; set; }
        public DateTime? DatePesage { get; set; }
        public int? IdUser { get; set; }

        public Personne IdPersonneNavigation { get; set; }
        public Users IdUserNavigation { get; set; }

    }
}
