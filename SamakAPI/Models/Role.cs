﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SamakAPI.Models
{
    public class Role
    {
        public Role()
        {
            Users = new HashSet<Users>();
        }
        public int Id { get; set; }
        public string Libelle { get; set; }
        public ICollection<Users> Users { get; set; }
    }
}
