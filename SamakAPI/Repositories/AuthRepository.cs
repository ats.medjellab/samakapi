﻿using Microsoft.EntityFrameworkCore;
using SamakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SamakAPI.Repositories
{
    public class AuthRepository : IAuthRepository
    {
        private readonly SamakDBContext _context;

        public AuthRepository(SamakDBContext context)
        {
            _context = context;
        }

        public async Task<Users> Login(string username, string password)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.Username.ToLower() == username.ToLower() && x.Password.ToLower() == password.ToLower());
            if (user == null)
                return null;

            return user;
        }

        public async Task<Users> Update(Users user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<bool> UserExists(string username)
        {
            if (await _context.User.AnyAsync(x => x.Username == username))
                return true;
            return false;
        }


        public async Task<Role> GetRole(Users user)
        {
            return await _context.Role.FirstOrDefaultAsync(x => x.Id == user.IdRole);
        }


       
        public async Task<Users> GetUserByUsername(string username)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.Username.ToLower() == username.ToLower());
            if (user == null)
                return null;

            return user;
        }
    }
    public interface IAuthRepository
    {
        Task<Users> Login(string username, string password);
        Task<Users> GetUserByUsername(string username);
        Task<Users> Update(Users user);
        Task<Role> GetRole(Users user);
        Task<bool> UserExists(string username);
    }
}
