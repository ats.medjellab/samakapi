﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SamakAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "badge",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NumBadge = table.Column<string>(maxLength: 100, nullable: true),
                    NumSerie = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_badge", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    libelle = table.Column<string>(unicode: false, maxLength: 80, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "personne",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    matricule = table.Column<string>(maxLength: 200, nullable: false),
                    nom = table.Column<string>(maxLength: 100, nullable: false),
                    prenom = table.Column<string>(maxLength: 100, nullable: true),
                    phone = table.Column<string>(maxLength: 14, nullable: true),
                    cin = table.Column<string>(maxLength: 100, nullable: true),
                    cnss = table.Column<string>(maxLength: 100, nullable: true),
                    ramed = table.Column<string>(maxLength: 100, nullable: true),
                    dateNaissance = table.Column<DateTime>(type: "date", nullable: true),
                    situation = table.Column<string>(maxLength: 100, nullable: true),
                    ville = table.Column<string>(maxLength: 100, nullable: true),
                    adresse = table.Column<string>(maxLength: 100, nullable: true),
                    nombreEnfant = table.Column<int>(nullable: true),
                    dateEmbauche = table.Column<DateTime>(type: "date", nullable: true),
                    dateArret = table.Column<DateTime>(type: "date", nullable: true),
                    IDBadge = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personne", x => x.id);
                    table.ForeignKey(
                        name: "FK__personne__IDBadg__49C3F6B7",
                        column: x => x.IDBadge,
                        principalTable: "badge",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nom = table.Column<string>(maxLength: 100, nullable: true),
                    prenom = table.Column<string>(maxLength: 100, nullable: true),
                    username = table.Column<string>(maxLength: 100, nullable: true),
                    password = table.Column<string>(maxLength: 100, nullable: true),
                    phone = table.Column<string>(maxLength: 14, nullable: true),
                    idRole = table.Column<int>(nullable: true),
                    dateLogin = table.Column<DateTime>(type: "datetime", nullable: true),
                    dateLogout = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.id);
                    table.ForeignKey(
                        name: "FK__users__idrole__71D1E811",
                        column: x => x.idRole,
                        principalTable: "role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "pesage",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    idPersonne = table.Column<int>(nullable: false),
                    poids = table.Column<double>(nullable: false),
                    datePesage = table.Column<DateTime>(type: "datetime", nullable: true),
                    idUser = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pesage", x => x.id);
                    table.ForeignKey(
                        name: "FK__pesage__idPerson__164452B1",
                        column: x => x.idPersonne,
                        principalTable: "personne",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__pesage__idUser__34C8D9D1",
                        column: x => x.idUser,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_personne_IDBadge",
                table: "personne",
                column: "IDBadge");

            migrationBuilder.CreateIndex(
                name: "UQ__personne__30962D11C7DD0BF2",
                table: "personne",
                column: "matricule",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_pesage_idPersonne",
                table: "pesage",
                column: "idPersonne");

            migrationBuilder.CreateIndex(
                name: "IX_pesage_idUser",
                table: "pesage",
                column: "idUser");

            migrationBuilder.CreateIndex(
                name: "IX_users_idRole",
                table: "users",
                column: "idRole");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "pesage");

            migrationBuilder.DropTable(
                name: "personne");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "badge");

            migrationBuilder.DropTable(
                name: "role");
        }
    }
}
